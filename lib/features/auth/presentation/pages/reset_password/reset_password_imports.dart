import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tdd/core/constants/CustomButtonAnimation.dart';
import 'package:flutter_tdd/features/auth/presentation/pages/reset_password/widgets/reset_password_widgets_imports.dart';
import 'package:flutter_tdd/features/auth/presentation/widgets/build_auth_app_bar.dart';

import '../../widgets/build_header_title.dart';

part 'reset_password.dart';
part 'reset_password_controller.dart';