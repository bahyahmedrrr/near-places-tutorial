import 'package:dartz/dartz.dart';
import 'package:flutter_tdd/core/errors/failures.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/models/model_to_domain/model_to_domain.dart';
import 'package:flutter_tdd/features/auth/data/models/user_model/user_model.dart';
import 'package:flutter_tdd/features/auth/domain/models/user_domain_model.dart';
import 'package:flutter_tdd/features/base/data/data_sources/home_remote_data_source.dart';
import 'package:flutter_tdd/features/base/data/models/place_model.dart';
import 'package:flutter_tdd/features/base/domain/entity/places_params.dart';
import 'package:flutter_tdd/features/base/domain/repositories/base_repository.dart';
import 'package:injectable/injectable.dart';


@Injectable(as: BaseRepository)
class ImplBaseRepository extends BaseRepository with ModelToDomain {

  @override
  Future<Either<Failure, UserDomainModel>> getUser(bool param)async {
    var result = await getIt.get<HomeRemoteDataSource>().getUser(param);
    return toDomainResult<UserDomainModel, UserModel>(result);
  }

  @override
  Future<Either<Failure, List<PlaceModel>>> getPlaces(PlacesParams params)async {
   return getIt<HomeRemoteDataSource>().getPlaces(params);
  }



}