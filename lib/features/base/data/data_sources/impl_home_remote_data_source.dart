// ignore_for_file: avoid_dynamic_calls

import 'package:dartz/dartz.dart';
import 'package:flutter_tdd/core/errors/failures.dart';
import 'package:flutter_tdd/core/http/generic_http/api_names.dart';
import 'package:flutter_tdd/core/http/generic_http/generic_http.dart';
import 'package:flutter_tdd/core/http/models/http_request_model.dart';
import 'package:flutter_tdd/features/auth/data/models/user_model/user_model.dart';
import 'package:flutter_tdd/features/base/data/models/place_model.dart';
import 'package:flutter_tdd/features/base/domain/entity/places_params.dart';
import 'package:injectable/injectable.dart';

import 'home_remote_data_source.dart';

@Injectable(as: HomeRemoteDataSource)
class ImplHomeRemoteDataSource extends HomeRemoteDataSource {

  @override
  Future<Either<Failure, UserModel>> getUser(bool param) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.ADS,
      requestMethod: RequestMethod.get,
      responseType: ResType.list,
      refresh: param,
      toJsonFunc: (json) => UserModel.fromJson(json),
    );
    return await GenericHttpImpl<UserModel>()(model);
  }

  @override
  Future<Either<Failure, List<PlaceModel>>> getPlaces(PlacesParams params) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.nearByPlaces+params.toQuery(),
      requestMethod: RequestMethod.get,
      responseType: ResType.list,
      showLoader: true,
      toJsonFunc:  (json) => List<PlaceModel>.from(
        json.map(
              (e) => PlaceModel.fromJson(e),
        ),
      ),
      responseKey: (data) => data["results"],
      errorFunc: (data)=> data["msg"],
    );
    return await GenericHttpImpl<List<PlaceModel>>().call(model);
  }


}
