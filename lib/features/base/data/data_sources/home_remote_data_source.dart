import 'package:dartz/dartz.dart';
import 'package:flutter_tdd/core/errors/failures.dart';
import 'package:flutter_tdd/features/auth/data/models/user_model/user_model.dart';
import 'package:flutter_tdd/features/base/data/models/place_model.dart';
import 'package:flutter_tdd/features/base/domain/entity/places_params.dart';

abstract class HomeRemoteDataSource {
  Future<Either<Failure, UserModel>> getUser(bool param);
  Future<Either<Failure, List<PlaceModel>>> getPlaces (PlacesParams params);
}