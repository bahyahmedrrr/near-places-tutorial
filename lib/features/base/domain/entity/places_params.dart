class PlacesParams {
  double lat ;
  double lng ;
  String key ;
  PlacesParams({required this.lat , required this.lng, required this.key});

  String toQuery ()=> "?location=$lat,$lng&radius=15&key=$key";
}