
import 'package:dartz/dartz.dart';
import 'package:flutter_tdd/core/errors/failures.dart';
import 'package:flutter_tdd/features/auth/domain/models/user_domain_model.dart';
import 'package:flutter_tdd/features/base/data/models/place_model.dart';
import 'package:flutter_tdd/features/base/domain/entity/places_params.dart';

abstract class BaseRepository {
  Future<Either<Failure, UserDomainModel>> getUser(bool param);
  Future<Either<Failure, List<PlaceModel>>> getPlaces (PlacesParams params);

}