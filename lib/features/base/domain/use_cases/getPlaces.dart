import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/base/data/models/place_model.dart';
import 'package:flutter_tdd/features/base/domain/entity/places_params.dart';
import 'package:flutter_tdd/features/base/domain/repositories/base_repository.dart';

class GetPlacesUseCase implements UseCase<List<PlaceModel>, PlacesParams> {
  @override
  Future<List<PlaceModel>> call(PlacesParams params) async {
    var result = await getIt<BaseRepository>().getPlaces(params);
    return result.fold(
      (l) => [],
      (r) => r,
    );
  }
}
