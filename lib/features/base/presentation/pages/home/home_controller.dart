// ignore_for_file: use_build_context_synchronously

part of 'home_imports.dart';

class HomeController {
  final GenericBloc<List<PlaceModel>> placesBloc = GenericBloc([]);
  late TextEditingController address = TextEditingController();
  final LocationCubit locationCubit = LocationCubit();
  final GenericBloc<int> homeTabCubit = GenericBloc(0);
  late AnimationController animationController;
  late TabController tabController;
  late Animation<double> animation;
  late CurvedAnimation curve;

  List<IconData> tabs = [
    Icons.home,
    Icons.map_outlined,
    Icons.face,
  ];

  void initBottomNavigation(TickerProvider ticker) {
    tabController = TabController(length: 3, vsync: ticker);
  }

  void animateTabsPages(int index, BuildContext context) {
    if (index != homeTabCubit.state.data) {
      homeTabCubit.onUpdateData(index);
      tabController.animateTo(index);
    }
  }

  void onLocationClick(BuildContext context) async {
    getIt<LoadingHelper>().showLoadingDialog();
    var loc = await getIt<Utilities>().getCurrentLocation();
    locationCubit.onLocationUpdated(
      LocationEntity(
        lat: loc?.latitude ?? 24.774265,
        lng: loc?.longitude ?? 46.738586,
        address: "",
      ),
    );
    EasyLoading.dismiss();
    await Navigator.of(context).push(
      CupertinoPageRoute(
        builder: (cxt) => BlocProvider.value(
          value: locationCubit,
          child: const LocationAddress(),
        ),
      ),
    );
    getNearByPlaces();
  }

  Future<void> getNearByPlaces ()async {
    var params = _placesParams();
    var data = await GetPlacesUseCase().call(params);
    placesBloc.onUpdateData(data);
  }

  PlacesParams _placesParams (){
    return PlacesParams(
      lat: locationCubit.state.model!.lat,
      lng: locationCubit.state.model!.lng,
      key: "AIzaSyCkLIWyrN4D9OA23J6aKad_J2ftTxfxAS8"
    );
  }
}