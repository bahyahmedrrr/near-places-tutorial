part of 'home_imports.dart';

@RoutePage()
class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final HomeController controller = HomeController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DefaultAppBar(title: "Home"),
      body: ListView(
        padding: const EdgeInsets.symmetric(vertical: 10,),
        children: [
          BlocConsumer<LocationCubit, LocationState>(
            bloc: controller.locationCubit,
            listener: (context, state) {
              controller.address.text = state.model?.address ?? "";
              controller.locationCubit.onLocationUpdated(state.model!);
            },
            builder: (context, state) {
              return GenericTextField(
                  controller: controller.address,
                  fieldTypes: FieldTypes.clickable,
                  textColor: context.colors.primary,
                  onTab: () => controller.onLocationClick(context),
                  type: TextInputType.text,
                  action: TextInputAction.next,
                  validate: (value) => value!.validateEmpty(),
                  label: "Location",
                  margin: const EdgeInsets.only(
                          top: 10, bottom: 10, left: 10, right: 10)
                      .r,
                  prefixIcon: Icon(
                    Icons.gps_fixed,
                    color: context.colors.primary,
                  ));
            },
          ),
          BlocBuilder<GenericBloc<List<PlaceModel>>,
              GenericState<List<PlaceModel>>>(
            bloc: controller.placesBloc,
            builder: (context, state) {
              return Column(
                children: List.generate(state.data.length, (index) => Container(
                  margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25),
                    border: Border.all(
                      color: context.colors.primary
                    )
                  ),
                  padding: const EdgeInsets.symmetric(
                    horizontal: 10,
                    vertical: 15
                  ),
                  child: Row(
                    children: [
                      Icon(Icons.gps_fixed_sharp,color: context.colors.primary),
                      Gaps.hGap12,
                      Text(
                        state.data[index].name!,
                        style: const AppTextStyle.s14_w400(color: Colors.black),
                      ),
                    ],
                  ),
                ))
              );
            },
          )
        ],
      ),
    );
  }
}
